// ****************************
// *  Kéa_ToucheLibre_ISO88   *
// ****************************
//
// Kéa_ToucheLibre_ISO88 for keyboard ISO 88 keys (keyboard ISO 105 keys is recommended for best experience.)
//
// To put this code in this file : /usr/share/X11/xkb/symbols/fr
//
// ToucheLibre Keyboard is truly ergonomic keyboard designed with free philosophy.
// Kéa is a Bépo variant disposition for the ToucheLibre Keyboard.
// Kéa, like Bépo, is improved ergonomic french keymap using Dvorak method.
//
// Kéa is tuned for french, english and spanish languages (the 3 european languages with international status). 
// All the language with latin alphabet can be whritten with Kéa. 
// And a lot of language get a not so bad optimization about frequency letter disposition (always better than Qwerty).
//
// ToucheLibre Keyboard is designed for a good accessibility to scientific symbol and phonetical alphabet.
//
// see http://touchelibre.fr
// Version : V1.0 in mars 2019
// by Lilian Tribouilloy <lilian@touchelibre.fr>
// by Lilian Tribouilloy <lilian.tribouilloy@laposte.net>
//
//
// ┌─────┐
// │ S A │   S = Shift,  A = AltGr + Shift
// │ s a │   s = direct, a = AltGr
// └─────┘
//				AltGr2 is not available at the moment. So a lot of key is not available.
//				This default could be corrected by the key « ⎄ compose ». ⎄ need to be activated. 
// 				AltGr2 = 2 × ⎄
//				The line E is modified because the ToucheLibre's Numpad do not existe in ISO keyboard.
//				So, 1234567890 are sited in direct way instead of special key.
//				Only key «ç» ( <TLDE> ) is not in same place than on ToucheLibre keyboard.
//				(ç) on right MAJ could give a better site for key «ç».
//
// ┌─────┲━━━━━┯━━━━━┯━━━━━┯━━━━━┯━━━━━┳━━━━━┯━━━━━┳━━━━━┯━━━━━┯━━━━━┯━━━━━┯━━━━━┳━━━━━━━━━┓
// │ Ç ˛ ║ § ` │ / | │ $ € │ @ ∞ │ " † ║ ‹ ‘ │ › ’ ║ - — │ < ± │ > × │ % ÷ │ = ¤ ║ ⌫ Retour┃
// │ ç ¸ ║ 1 ~ │ 2 \ │ 3 & │ 4 # │ 5 ' ║ « “ │ » ” ║ 6 − │ 7 + │ 8 * │ 9 ° │ 0 ^ ║  arrière┃
// ┢━━━━━┻═┳═━═┷═┯═━═┷═┯═━═┷═┯═━═┷═┯═━═┻═┱───┴─┬───┺═┳═━═┷═┯═━═┷═┯═━═┷═┯═━═┷═┯═━═┻━┳━━━━━━━┫
// ┃   ↹   ┃ K  ͜  │ É ˝ │ P ̺  │ U Ù │ V ̭  ║ [ ⟨ │ ] ⟩ ║ L ɬ │ C Ɔ │ D Ð │ M ̱  │ Z ̮  ┃       ┃
// ┃  Tab  ┃ k ͡ │ é ˊ  │ p ̪  │ u ù │ v ˇ ║ ( { │ ) } ║ l / │ c ɔ │ d ð │ m ¯ │ z ̑  ┃Entrée ┃
// ┣━━━━━━━┻┱────┴┬────┴┬────┴┬────┴┬────┺┱────┴┬────┺┱────┴┬────┴┬────┴┬────┴┬────┺┓      ┃
// ┃   ⇬    ┃ È ̏  │ O Œ │ A Æ │ I ˙ │ E Ə ║ ; ̛ │ : · ║ R ʁ │ S ẞ │ T Þ │ N ̰  │ X ̟  ┃  ⏎   ┃
// ┃ M Lock ┃ è ` │ o œ │ a æ │ i ̈  │ e ə ║ , , │ . … ║ r ɹ │ s ß │ t þ │ n ˜ │ x ̽  ┃      ┃
// ┣━━━━━━━┳┹────┬┴────┬┴────┬┴────┬┴────┲┹────┬┴────┲┹────┬┴────┬┴────┬┴────┲┷━━━━━┻━━━━━━┫
// ┃   ⇧   ┃ À ̥  │ F Ʃ │ Y ʎ │ J Ʒ │ W ʍ ║ ! ̣  │ ? ̉  ║ H ɧ │ G ɲ │ B ǂ │ Q ̢ ┃     ¦  ⇧    ┃
// ┃  MAJ  ┃ à ˚ │ f ʃ │ y ɥ │ j ʒ │ w ̆  ║ ’ ¡ │ ˆ ¿ ║ h ̵ │ g µ │ b ǁ │ q ̡ ┃ (ç) ¦ MAJ   ┃
// ┣━━━━━┳━┻━━━┳━┷━━━┳━┷━━━┱─┴─────┴─────┺━━━━━┷━━━━━┹─────┴───┲━┷━━━━━╈━━━━━┻━┳━━━━━━━━━━━┫
// ┃  ⎈  ┃     ┃  ⌘  ┃  ⌥  ┃   ½⍽  ½ spc inséc.  ⍽ spc inséc.  ┃   ⇮   ┃       ┃     ↑     ┃
// ┃Ctrl ┃ Fn  ┃Meta ┃ Alt ┃   ␣   Espace        _ under score ┃ AltGr ┃ Compo ┃  ←  ↓  →  ┃
// ┗━━━━━┻━━━━━┻━━━━━┻━━━━━┹───────────────────────────────────┺━━━━━━━┻━━━━━━━┻━━━━━━━━━━━┛
//
// ***********************
//
partial alphanumeric_keys
xkb_symbols "bepo" {

    include "level3(ralt_switch)"
    include "keypad(oss)"
    include "level5(modifier_mapping)"
    include "shift(both_capslock)"

    key.type[Group1] = "EIGHT_LEVEL";

    key <MENU> { type[Group1]="ONE_LEVEL", symbols[Group1] = [ ISO_Level5_Shift ] };

    name[Group1]= "French (Bepo, ergonomic, Dvorak way)";

// First row
    key <TLDE> { [        ccedilla,             Ccedilla,       dead_cedilla,      dead_ogonek  ] }; // ç Ç ¸ ˛

    key <AE01> { [               1,              section,         asciitilde,            grave  ] }; // 1 § ~ `
    key <AE02> { [               2,                slash,          backslash,              bar  ] }; // 2 / \ |
    key <AE03> { [               3,               dollar,          ampersand,         EuroSign  ] }; // 3 $ & €
    key <AE04> { [               4,                   at,         numbersign,            U221E  ] }; // 4 @ # ∞
    key <AE05> { [               5,             quotedbl,         apostrophe,           dagger  ] }; // 5 " ' †

    key <AE06> { [   guillemotleft,         U2039,   leftdoublequotemark,  leftsinglequotemark  ] }; // « ‹ “ ‘
    key <AE07> { [  guillemotright,         U203A,  rightdoublequotemark, rightsinglequotemark  ] }; // » › ” ’

    key <AE08> { [               6,                minus,              U2212,           emdash  ] }; // 6 - − —
    key <AE09> { [               7,                 less,               plus,        plusminus  ] }; // 7 < + ± 
    key <AE10> { [               8,              greater,           asterisk,         multiply  ] }; // 8 > * ×
    key <AE11> { [               9,              percent,             degree,         division  ] }; // 9 % ° ÷
    key <AE12> { [               0,                equal,        asciicircum,    dead_currency  ] }; // 0 = ^ ¤

// Second row
    key <AD01> { [               k,                    K,              U0361,            U035C  ] }; // k K ͡   ͜ 
    key <AD02> { [          eacute,               Eacute,         dead_acute, dead_doubleacute  ] }; // é É ˊ ˝
    key <AD03> { [               p,                    P,              U032A,            U033A  ] }; // p P ̪  ̺
    key <AD04> { [               u,                    U,             ugrave,           Ugrave  ] }; // u U ù Ù
    key <AD05> { [               v,                    V,         dead_caron,            U032D  ] }; // v V ˇ ̭

    key <AD06> { [       parenleft,          bracketleft,          braceleft,            U27E8  ] }; // ( [ { ⟨
    key <AD07> { [      parenright,         bracketright,         braceright,            U27E9  ] }; // ) ] } ⟩

    key <AD08> { [               l,                    L,        dead_stroke,            U026C  ] }; // l L / ɬ
    key <AD09> { [               c,                    C,              U0254,            U0186  ] }; // c C ɔ Ɔ
    key <AD10> { [               d,                    D,                eth,              ETH  ] }; // d D ð Ð
    key <AD11> { [               m,                    M,        dead_macron,            U0331  ] }; // m M ̄  ̱
    key <AD12> { [               z,                    Z,              U0311,            U032E  ] }; // z Z ̑  ̮ 

// Third row
    key <AC01> { [          egrave,               Egrave,         dead_grave,            U030F  ] }; // è È ` ̏
    key <AC02> { [               o,                    O,                 oe,               OE  ] }; // o O œ Œ
    key <AC03> { [               a,                    A,                 ae,               AE  ] }; // a A æ Æ
    key <AC04> { [               i,                    I,     dead_diaeresis,    dead_abovedot  ] }; // i I ̈ ˙
    key <AC05> { [               e,                    E,              schwa,            SCHWA  ] }; // e E ə Ə

    key <AC06> { [           comma,            semicolon,    dead_belowcomma,        dead_horn  ] }; // , ; , ̛
    key <AC07> { [          period,                colon,           ellipsis,   periodcentered  ] }; // . : … ·

    key <AC08> { [               r,                    R,              U0279,            U0281  ] }; // r R ɹ ʁ
    key <AC09> { [               s,                    S,             ssharp,            U1E9E  ] }; // s S ß ẞ
    key <AC10> { [               t,                    T,              thorn,            THORN  ] }; // t T þ Þ
    key <AC11> { [               n,                    N,         dead_tilde,            U0330  ] }; // n N ̃  ̰
    key <BKSL> { [               x,                    X,              U033D,            U031F  ] }; // x X ̽  ̟

// Fourth row
    key <LSGT> { [          agrave,               Agrave,     dead_abovering,            U0325  ] }; // à À ˚ ̥
    key <AB01> { [               f,                    F,              U0283,            U01A9  ] }; // f F ʃ Ʃ
    key <AB02> { [               y,                    Y,              U0265,            U028E  ] }; // y Y ɥ ʎ
    key <AB03> { [               j,                    J,              U0292,            U01B7  ] }; // j J ʒ Ʒ
    key <AB04> { [               w,                    W,         dead_breve,            U028D  ] }; // w W ̆  ʍ

    key <AB05> { [ rightsinglequotemark,          exclam,         exclamdown,    dead_belowdot  ] }; // ’ ! ¡ ̣
    key <AB06> { [ dead_circumflex,             question,       questiondown,        dead_hook  ] }; // ^ ? ¿ ̉

    key <AB07> { [               h,                    H,              U0335,            U0267  ] }; // h H ̵ ɧ
    key <AB08> { [               g,                    G,         dead_greek,            U0272  ] }; // g G µ ɲ
    key <AB09> { [               b,                    B,              U01C1,            U01C2  ] }; // b B ǁ ǂ
    key <AB10> { [               q,                    Q,              U0321,            U0322  ] }; // q Q ̡  ̢

// Fifth row
    key <SPCE> { [           space,                U202F,         underscore,     nobreakspace  ] }; // ␣ ½⍽ _ ⍽
};
//
// _____________________________________________________________________________________________________
//
// ¡¡¡   To put this code in this file : /usr/share/X11/xkb/symbols/fr   !!!
//
// ******************************
// *  Kéa_ToucheLibre_ISO105    *
// ******************************
//
// Kéa_ToucheLibre_ISO105 for keyboard ISO 105 (french layout for example)
//
// ToucheLibre Keyboard is truly ergonomic keyboard designed with free philosophy.
// Kéa is a Bépo variant disposition for the ToucheLibre Keyboard.
// Kéa, like Bépo, is improved ergonomic french keymap using Dvorak method.
//
// Kéa is tuned for french, english and spanish languages (the 3 european languages with international status). 
// All the language with latin alphabet can be whritten with Kéa. 
// And a lot of language get a not so bad optimization about frequency letter disposition (always better than Qwerty).
//
// ToucheLibre Keyboard is designed for a good accessibility to scientific symbol and phonetical alphabet.
//
// see http://touchelibre.fr
// Not integrated in XOrg
// Version : V1.0 in mars 2019
// by Lilian Tribouilloy <lilian@touchelibre.fr>
//
//
//
// ********** First Table **********
//
// ┌─────┐
// │ S A │   S = Shift,  A = AltGr + Shift
// │ s a │   s = direct, a = AltGr
// └─────┘
//			Same layout than Kéa_ToucheLibre_ISO88 but first row and keypad are modified.
//			This layout is very close to the original Kéa for keyboard ToucheLibre.
//			So this layout should be proefered to Kéa_ToucheLibre_ISO88.
//
// ┌─────┲━━━━━┯━━━━━┯━━━━━┯━━━━━┯━━━━━┳━━━━━┯━━━━━┳━━━━━┯━━━━━┯━━━━━┯━━━━━┯━━━━━┳━━━━━━━━━┓  ┏━━━━━┱─────┬─────┬─────┐
// │ Ç ˛ ║ ` ¶ │ \ ¦ │ & ☹ │ # ✆ │ ' ‡ ║ ‹ ‘ │ › ’ ║ - – │ ⩽ ♀ │ ⩾ ♂ │ ° ″ │ ® ™ ║ ⌫ Retour┃  ┃ e √ ┃ ÷ ¬ │ × ∘ │ − ∂ │
// │ ç ¸ ║ ~ § │ / | │ $ ☺ │ @ ✉ │ " † ║ « “ │ » ” ║ -⍽ —│ < ≪ │ > ≫ │ % ′ │ © 🄯 ║  arrière┃  ┃ ^ ⏨  ┃ / : │ * ⋅ │ - ∓ │
// ┢━━━━━┻═┳═━═┷═┯═━═┷═┯═━═┷═┯═━═┷═┯═━═┻═┱───┴─┬───┺═┳═━═┷═┯═━═┷═┯═━═┷═┯═━═┷═┯═━═┻━┳━━━━━━━┫  ┡━━━━━╃─────┼─────┼─────┤
// ┃   ↹   ┃ K  ͜  │ É ˝ │ P ̺  │ U Ù │ V ̭  ║ [ ⟨ │ ] ⟩ ║ L ɬ │ C Ɔ │ D Ð │ M ̱  │ Z ̮  ┃       ┃  │ ( ℂ │ ) ℍ │ ∞ ∃ │     │
// ┃  Tab  ┃ k ͡ │ é ˊ  │ p ̪  │ u ù │ v ˇ ║ ( { │ ) } ║ l / │ c ɔ │ d ð │ m ¯ │ z ̑  ┃Entrée ┃  │ 7 i │ 8 j │ 9 ∀ │     │
// ┣━━━━━━━┻┱────┴┬────┴┬────┴┬────┴┬────┺┱────┴┬────┺┱────┴┬────┴┬────┴┬────┴┬────┺┓      ┃  ├─────┼─────┼─────┤ ± ∫ │
// ┃   ⇬    ┃ È ̏  │ O Œ │ A Æ │ I ˙ │ E Ə ║ ; ̛ │ : · ║ R ʁ │ S ẞ │ T Þ │ N ̰  │ X ̟  ┃  ⏎   ┃  │ D ⅅ │ E ℚ │ F ℝ │ + ∑ │
// ┃ Level5 ┃ è ` │ o œ │ a æ │ i ̈  │ e ə ║ , , │ . … ║ r ɹ │ s ß │ t þ │ n ˜ │ x ̽  ┃      ┃  │ 4 → │ 5 ⇒ │ 6 ⇔ │     │
// ┣━━━━━━━┳┹────┬┴────┬┴────┬┴────┬┴────┲┹────┬┴────┲┹────┬┴────┬┴────┬┴────┲┷━━━━━┻━━━━━━┫  ├─────┼─────┼─────╆━━━━━┪
// ┃   ⇧   ┃ À ̥  │ F Ʃ │ Y ʎ │ J Ʒ │ W ʍ ║ ! ⚠ │ ? ⯑ ║ H ɧ │ G ɲ │ B ǁ │ Q ̢ ┃     ¦  ⇧    ┃  │ A ℙ │ B ℕ │ C ℤ ┃     ┃
// ┃  MAJ  ┃ à ˚ │ f ʃ │ y ɥ │ j ʒ │ w ̆  ║ ’ ¡ │ ˆ ¿ ║ h ̵ │ g µ │ b ǀ │ q ̡ ┃ (ç) ¦ MAJ   ┃  │ 1 € │ 2 $ │ 3 ¤ ┃     ┃
// ┣━━━━━┳━┻━━━┳━┷━━━┳━┷━━━┱─┴─────┴─────┺━━━━━┷━━━━━┹─────┴───┲━┷━━━━━╈━━━━━┻━┳━━━━━━━━━━━┫  ├─────┴─────┼─────┨ ≈ ≝ ┃
// ┃  ⎈  ┃     ┃  ⌘  ┃  ⌥  ┃   ½⍽  ½ spc inséc.  ⍽ spc inséc.  ┃   ⇮   ┃       ┃     ↑     ┃  │    x ∅    │ . ∈ ┃ = ≠ ┃
// ┃Ctrl ┃ Fn  ┃Meta ┃ Alt ┃   ␣   Espace        _ under score ┃ AltGr ┃ Compo ┃  ←  ↓  →  ┃  │    0 b    │ , ½⍽┃     ┃
// ┗━━━━━┻━━━━━┻━━━━━┻━━━━━┹───────────────────────────────────┺━━━━━━━┻━━━━━━━┻━━━━━━━━━━━┛  └───────────┴─────┺━━━━━┛
//
//
//
// ********** Second Table **********
//
// ┌─────┐
// │ 6 G │   6 = Level 6 = Shift + level 5,		G = Greek + Shift
// │ 5 g │   5 = level 5,						g = Greek
// └─────┘
//			level 5 is done with Maj Lock key.
//			New Maj Lock is done with both shift in same time.
//			Greek is the dead_greek key = AltGr + g
//			L3 (AltGr) + L5 = L7	and		Shift + L3 + L5 = L8 could be added.
//
// ┌─────┲━━━━━┯━━━━━┯━━━━━┯━━━━━┯━━━━━┳━━━━━┯━━━━━┳━━━━━┯━━━━━┯━━━━━┯━━━━━┯━━━━━┳━━━━━━━━━┓  ┏━━━━━┱─────┬─────┬─────┐
// │ Ꞌ Ϙ ║ ☑ Ͼ │⛔ Ͽ │ ☠ Ϗ │ 🍀 Ͱ │ ⚔ ϵ ║ ‟ ̞  │ ‛ ̙  ║ ⧫ ϕ │ ≼ Ϛ │ ≽ Ͳ │ º Ϻ │ ℗ Ͷ ║ ⌫ Retour┃  ┃     ┃     │     │     │
// │ ꞌ ϙ ║ ☐ ͼ │◌ ⃠ ͽ │ ♥ ϗ │ 💡 ͱ │ ⛏ ϖ ║ „ ̝  │ ‚ ̘  ║ ▶ ϱ │ ≺ ϛ │ ≻ ͳ │ ª ϻ │ 🛈 ͷ ║  arrière┃  ┃     ┃     │     │     │
// ┢━━━━━┻═┳═━═┷═┯═━═┷═┯═━═┷═┯═━═┷═┯═━═┻═┱───┴─┬───┺═┳═━═┷═┯═━═┷═┯═━═┷═┯═━═┷═┯═━═┻━┳━━━━━━━┫  ┡━━━━━╃─────┼─────┼─────┤
// ┃   ↹   ┃ ̯  Κ │ ɵ Ϝ │ ̚ Π │ ɰ Υ │ Ʌ ϴ ║ ⌈   │ ⌉  ᷅  ║ ̴ Λ │◌ɕ ϐ │ ̹  Δ │ ̼  Μ │ Ȝ Ζ ┃       ┃  │     │     │     │     │
// ┃  Tab  ┃ ̩  κ │ ɘ ϝ │ ̻  π │ ɯ υ │ ʌ ϑ ║ ⌊  ͅ  │ ⌋  ᷄  ║ ɮ λ │ ɕ ς │ ̜  δ │ ɱ μ │ ȝ ζ ┃Entrée ┃  │     │     │     │     │
// ┣━━━━━━━┻┱────┴┬────┴┬────┴┬────┴┬────┺┱────┴┬────┺┱────┴┬────┴┬────┴┬────┴┬────┺┓      ┃  ├─────┼─────┼─────┤     │
// ┃   ⇬    ┃ ɞ Ϡ │ ʘ Ο │ ɒ Α │◌̣  Ι │◌˞ Ε ║ ˑ   │ ◼  ᷉  ║ ɺ Ρ │ ſ Σ │ Ƿ Τ │ Ŋ Ν │ Ȣ  Χ ┃  ⏎   ┃  │     │     │     │     │
// ┃ Level5 ┃ ɜ ϡ │◌⤉ ο │ ɑ α │ ̤  ι │ ɚ ε ║ ː ̓  │ • ̍  ║ ɾ ρ │◌̉  σ │ ƿ τ │ ŋ ν │ ȣ  χ ┃      ┃  │     │     │     │     │
// ┣━━━━━━━┳┹────┬┴────┬┴────┬┴────┬┴────┲┹────┬┴────┲┹────┬┴────┬┴────┬┴────┲┷━━━━━┻━━━━━━┫  ├─────┼─────┼─────╆━━━━━┪
// ┃   ⇧   ┃ Ɐ Ϟ │ ƒ Φ │ Ƴ Ψ │ Ƹ Θ │ Ʊ Ω ║ ˌ   │ ‽   ║ ʕ Η │ ᵹ   Γ │ ǃ Β │ ɣ Ξ ┃     ¦  ⇧    ┃  │     │     │     ┃     ┃
// ┃  MAJ  ┃ ɐ ϟ │ ɸ φ │ ƴ ψ │ ƹ θ │ ʊ ω ║ ˈ ̔  │ ⸮ ◌̈́  ║ ʔ η │ ɡ γ │ ǂ β │ ɤ ξ ┃ (ç) ¦ MAJ   ┃  │     │     │     ┃     ┃
// ┣━━━━━┳━┻━━━┳━┷━━━┳━┷━━━┱─┴─────┴─────┺━━━━━┷━━━━━┹─────┴───┲━┷━━━━━╈━━━━━┻━┳━━━━━━━━━━━┫  ├─────┴─────┼─────┨     ┃
// ┃  ⎈  ┃     ┃  ⌘  ┃  ⌥  ┃   ⍽                               ┃   ⇮   ┃       ┃     ↑     ┃  │           │     ┃     ┃
// ┃Ctrl ┃ Fn  ┃Meta ┃ Alt ┃   ␣                               ┃ AltGr ┃ Compo ┃  ←  ↓  →  ┃  │           │     ┃     ┃
// ┗━━━━━┻━━━━━┻━━━━━┻━━━━━┹───────────────────────────────────┺━━━━━━━┻━━━━━━━┻━━━━━━━━━━━┛  └───────────┴─────┺━━━━━┛
//
//
//
// ********** Code Part *************
//
partial alphanumeric_keys
xkb_symbols "bepo_latin9" {

    include "level3(ralt_switch)"
    include "keypad(oss)"
    include "level5(modifier_mapping)"
    include "shift(both_capslock)"
    key.type[Group1] = "EIGHT_LEVEL";

    name[Group1]="French (Bepo, ergonomic, Dvorak way, Latin-9 only)";

// Level 5 definition
    key <CAPS> { type[Group1]="ONE_LEVEL", symbols[Group1] = [ ISO_Level5_Shift ] };

// First row
    key <TLDE> { [        ccedilla,     Ccedilla, dead_cedilla,   dead_ogonek,  UA78C,   UA78B  ] }; // ç Ç ¸ ˛ ꞌ Ꞌ
    key <AE01> { [      asciitilde,        grave,      section,         U00B6,  U2610,   U2611  ] }; // ~ ` § ¶ ☐ ☑
    key <AE02> { [           slash,    backslash,          bar,         U00A6,  U20E0,   U26D4  ] }; // / \ | ¦ ◌ ⃠ ⛔
    key <AE03> { [          dollar,    ampersand,        U263A,         U2639,  U2665,   U2620  ] }; // $ & ☺ ☹ ♥ ☠
    key <AE04> { [              at,   numbersign,        U2709,         U2706, U1F4A1,  U1F340  ] }; // @ # ✉ ✆ 💡 🍀
    key <AE05> { [        quotedbl,   apostrophe,       dagger,  doubledagger,  U26CF,   U2694  ] }; // " ' † ‡ ⛏ ⚔

    key <AE06> { [   guillemotleft,        U2039,        U201C,         U2018,  U201E,   U201F  ] }; // « ‹ “ ‘ „ ‟
    key <AE07> { [  guillemotright,        U203A,        U201D,         U2019,  U201A,   U201B  ] }; // » › ” ’ ‚ ‛

    key <AE08> { [           U2011,        minus,        U2212,        emdash,  U25B6,   U29EB  ] }; // -⍽ - − — ▶ ⧫
    key <AE09> { [            less,        U2A7D,        U226A,         U2640,  U227A,   U227C  ] }; // < ⩽ ≪ ♀ ≺ ≼
    key <AE10> { [         greater,        U2A7E,        U226B,         U2642,  U227B,   U227D  ] }; // > ⩾ ≫ ♂ ≻ ≽
    key <AE11> { [         percent,       degree,      minutes,       seconds,  U00AA,   U00BA  ] }; // % ° ′ ″ ª º
    key <AE12> { [       copyright,   registered,       U1F12F,     trademark, U1F6C8,   U2117  ] }; // © ® 🄯 ™ 🛈 ℗

// Second row
    key <AD01> { [               k,            K,        U0361,         U035C,  U0329,   U032F  ] }; // k K ͡   ͜   ̩  ̯ 
    key <AD02> { [          eacute,       Eacute,   dead_acute, dead_doubleacute, U0258, U0275  ] }; // é É ˊ ˝ ɘ ɵ
    key <AD03> { [               p,            P,        U032A,         U033A,  U033B,   U031A  ] }; // p P ̪  ̺  ̻  ̚
    key <AD04> { [               u,            U,       ugrave,        Ugrave,  U026F,   U0270  ] }; // u U ù Ù ɯ ɰ
    key <AD05> { [               v,            V,   dead_caron,         U032D,  U028C,   U0245  ] }; // v V ˇ ̭  ʌ Ʌ

    key <AD06> { [       parenleft,  bracketleft,   braceleft,          U27E8,  U230A,   U2308  ] }; // ( [ { ⟨ ⌊ ⌈
    key <AD07> { [      parenright, bracketright,  braceright,          U27E9,  U230B,   U2309  ] }; // ) ] } ⟩ ⌋ ⌉

    key <AD08> { [               l,           L,  dead_stroke,          U026C,  U026E,   U0334  ] }; // l L / ɬ ɮ ̴ 
    key <AD09> { [               c,           C,        U0254,          U0186,  U0255  ] }; // c C ɔ Ɔ ɕ ◌ɕ
    key <AD10> { [               d,           D,          eth,            ETH,  U031C,   U0339  ] }; // d D ð Ð ̜  ̹ 
    key <AD11> { [               m,           M,  dead_macron,          U0331,  U0271,   U033C  ] }; // m M ̄  ̱  ɱ ̼ 
    key <AD12> { [               z,           Z,        U0311,          U032E,  U021D,   U021C  ] }; // z Z ̑  ̮  ȝ Ȝ

// Third row
    key <AC01> { [          egrave,      Egrave,   dead_grave,       U030F,     U025C,   U025E  ] }; // è È ` ̏  ɜ ɞ
    key <AC02> { [               o,           O,           oe,          OE,           U0298  ] }; // o O œ Œ ◌⤉ ʘ
    key <AC03> { [               a,           A,           ae,          AE,     U0251,   U0252  ] }; // a A æ Æ ɑ ɒ
    key <AC04> { [               i,           I, dead_diaeresis, dead_abovedot, U0324,   U0323  ] }; // i I ̈ ˙ ̤  ◌̣ 
    key <AC05> { [               e,           E,        schwa,       SCHWA,     U025A,   U02DE  ] }; // e E ə Ə ɚ ◌˞

    key <AC06> { [           comma,   semicolon,  dead_belowcomma,  dead_horn,  U02D0,   U02D1  ] }; // , ; , ̛ ː ˑ
    key <AC07> { [          period,       colon,     ellipsis,  periodcentered, U2022,   U25FC  ] }; // . : … · • ◼

    key <AC08> { [               r,           R,        U0279,       U0281,     U027E,   U027A  ] }; // r R ɹ ʁ ɾ ɺ
    key <AC09> { [               s,           S,       ssharp,       U1E9E,     U0309,   U017F  ] }; // s S ß ẞ ◌̉ ſ
    key <AC10> { [               t,           T,        thorn,       THORN,     U01BF,   U01F7  ] }; // t T þ Þ ƿ Ƿ
    key <AC11> { [               n,           N,   dead_tilde,       U0330,     U014B,   U014A  ] }; // n N ̃  ̰  ŋ Ŋ
    key <BKSL> { [               x,           X,        U033D,       U031F,     U0223,   U0222  ] }; // x X ̽  ̟  ȣ Ȣ

// Fourth row
    key <LSGT> { [          agrave,      Agrave,  dead_abovering,    U0325,     U0250,   U2C6F  ] }; // à À ˚ ̥  ɐ Ɐ
    key <AB01> { [               f,           F,        U0283,       U01A9,     U0278,   U0192  ] }; // f F ʃ Ʃ ɸ ƒ
    key <AB02> { [               y,           Y,        U0265,       U028E,     U01B4,   U01B3  ] }; // y Y ɥ ʎ ƴ Ƴ
    key <AB03> { [               j,           J,        U0292,       U01B7,     U01B9,   U01B8  ] }; // j J ʒ Ʒ ƹ Ƹ
    key <AB04> { [               w,           W,   dead_breve,       U028D,     U028A,   U01B1  ] }; // w W ̆  ʍ ʊ Ʊ

    key <AB05> { [           U2019,      exclam,   exclamdown,       U26A0,     U02C8,   U02CC  ] }; // ’ ! ¡ ⚠ ˈ ˌ
    key <AB06> { [ dead_circumflex,    question, questiondown,       U2BD1,     U2E2E,   U203D  ] }; // ^ ? ¿ ⯑ ⸮ ‽

    key <AB07> { [               h,           H,        U0335,       U0267,     U0294,   U0295  ] }; // h H ̵ ɧ ʔ ʕ
    key <AB08> { [               g,           G,   dead_greek,       U0272,     U0261,   U1D79  ] }; // g G µ ɲ ɡ ᵹ
    key <AB09> { [               b,           B,        U01C0,       U01C1,     U01C2,   U01C3  ] }; // b B ǀ ǁ ǂ ǃ
    key <AB10> { [               q,           Q,        U0321,       U0322,     U0264,   U0263  ] }; // q Q ̡  ̢ ɤ ɣ

// Fifth row
    key <SPCE> { [           space,       U202F,   underscore,  nobreakspace,   U2423,   U237D  ] }; // ␣ ½⍽ _ ⍽ ␣ ⍽

};
//
// _________________________________________________________________________________________________
//
// ******************************
// *  Kéa_ToucheLibre_Keypad    *
// ******************************
//
// Keypad definition for ToucheLibre keyboard
// To be used with "Kéa_ToucheLibre_ISO105"
//
// see http://touchelibre.fr
// Not integrated in XOrg
// Version : V1.0 in mars 2019
// by Lilian Tribouilloy <lilian@touchelibre.fr>
//
//
// ********** First Table **********
//
// ┌─────┐
// │ S A │   S = Shift,  A = AltGr + Shift
// │ s a │   s = direct, a = AltGr
// └─────┘
//
// ┏━━━━━┱─────┬─────┬─────┐
// ┃ e √ ┃ ÷ ¬ │ × ∘ │ − ∂ │
// ┃ ^ ⏨  ┃ / : │ * ⋅ │ - ∓ │
// ┡━━━━━╃─────┼─────┼─────┤
// │ ( ℂ │ ) ℍ │ ∞ ∃ │     │
// │ 7 i │ 8 j │ 9 ∀ │     │
// ├─────┼─────┼─────┤ ± ∫ │
// │ D ⅅ │ E ℚ │ F ℝ │ + ∑ │
// │ 4 → │ 5 ⇒ │ 6 ⇔ │     │
// ├─────┼─────┼─────╆━━━━━┪
// │ A ℙ │ B ℕ │ C ℤ ┃     ┃
// │ 1 € │ 2 $ │ 3 ¤ ┃     ┃
// ├─────┴─────┼─────┨ ≈ ≝ ┃
// │    x ∅    │ . ∈ ┃ = ≠ ┃
// │    0 b    │ , ½⍽┃     ┃
// └───────────┴─────┺━━━━━┛
//
//
// ********** Second Table **********
//
// ┌─────┐
// │ 6 8 │   6 = Level 6 = Shift + level 5,		8 = level8 = AltGr + Shift + level5
// │ 5 7 │   5 = level 5,						7 = level7 = AltGr + level5
// └─────┘
//
// ┏━━━━━┱─────┬─────┬─────┐
// ┃ ℒ     ┃ Ш   │ ∗   │ ∇   │
// ┃ ℱ     ┃ ⧠      │ ∏   │ ∆   │
// ┡━━━━━╃─────┼─────┼─────┤
// │ ⊆   │ ⊇   │ ∩   │     │
// │ ⊂   │ ⊃   │ ∪   │     │
// ├─────┼─────┼─────┤ ∧   │
// │ ∋   │ ⇌   │ ℶ     │ ∨   │
// │ ∈   │ ↦   │ ℵ     │     │
// ├─────┼─────┼─────╆━━━━━┪
// │ ⫽       │ ⏦   │ ℑ     ┃     ┃
// │ ⟂   │ ⎓   │ ℜ     ┃     ┃
// ├─────┴─────┼─────┨ ≟   ┃
// │   ⬚       │ ⋮     ┃ ∝   ┃
// │   ◌       │ ⋯   ┃     ┃
// └───────────┴─────┺━━━━━┛
//
// ***********************
//
// ¡¡¡   To put this code in this file : /usr/share/X11/xkb/symbols/keypad   !!!
//

partial keypad_keys
xkb_symbols "core" {

    modifier_map Mod2 { Num_Lock };

    key <NMLK> { [     asciicircum,                    e,               U23E8,           U221A  ] }; // ^ e ⏨  √
    key <KPEN> { [           equal,                U2248,               U2260,           U225D  ] }; // = ≈ ≠ ≝
//    key <KPEQ> { [ KP_Equal                     ] };

};


partial keypad_keys
xkb_symbols "ossmath" {

    key.type[Group1] = "EIGHT_LEVEL";
//    key.type[Group1]="CTRL+ALT" ;

    key <KPDV> { [       KP_Divide,             division,             colon,             U00AC  ] }; //  / ÷ : ¬
    key <KPMU> { [     KP_Multiply,             multiply,             U22C5,             U2218  ] }; //  * × ⋅ ∘
    key <KPSU> { [     KP_Subtract,                U2212,             U2213,             U2202  ] }; //  - − ∓ ∂
    key <KPAD> { [          KP_Add,            plusminus,             U2211,             U222B  ] }; //  + ± ∑ ∫

};


partial keypad_keys
xkb_symbols "ossnumber" {

    key.type[Group1] = "EIGHT_LEVEL";
//    key.type[Group1]="FOUR_LEVEL_MIXED_KEYPAD" ;


    key <KP7>  { [               7,            parenleft,                 i,             U2102  ] }; //  7 ( i ℂ
    key <KP8>  { [               8,           parenright,                 j,             U210D  ] }; //  8 ) j ℍ
    key <KP9>  { [               9,                U221E,             U2200,             U2203  ] }; //  9 ∞ ∀ ∃

    key <KP4>  { [               4,                    D,             U2192,             U2145  ] }; //  4 D → ⅅ
    key <KP5>  { [               5,                    E,             U21D2,             U211A  ] }; //  5 E ⇒ ℚ
    key <KP6>  { [               6,                    F,             U21D4,             U211D  ] }; //  6 F ⇔ ℝ

    key <KP1>  { [               1,                    A,          EuroSign,             U2119  ] }; //  1 A € ℙ
    key <KP2>  { [               2,                    B,            dollar,             U2115  ] }; //  2 B $ ℕ 
    key <KP3>  { [               3,                    C,     dead_currency,             U2124  ] }; //  3 C ¤ ℤ

    key <KP0>  { [               0,                    x,                 b,             U2205  ] }; //  0 x b ∅

};


// ***********************
//
// ¡¡¡   To put this code in this file : /usr/share/X11/xkb/symbols/kpld   !!!
//

partial keypad_keys
xkb_symbols "dotoss" {

    key.type[Group1] = "EIGHT_LEVEL";
//    key.type[Group1]="FOUR_LEVEL_MIXED_KEYPAD" ;

    key <KPDL> { [           comma,               period,             U202F,             U2208  ] }; // , . ½⍽ ∈

};




*************************************
test / réserve / modif

direct :
~/$@"«»‑<>%©
képuv()lcdmz
èoaie,.rstnx
àfyjw’ôhgbqç

Maj :
`\&#'‹›-⩽⩾°®
KÉPUV[]LCDMZ
ÈOAIE;:RSTNX
ÀFYJW!?HGBQÇ

AltGr :
§|☺✉†“”−≪≫′🄯
e͡eó̪ùǒ{}łɔðē̑e
òœæöəș…ɹßþõ̽o
åʃɥʒŏ¡¿̵µǀ̡ş

AltGr+MAJ :
¶¦☹✆‡‘’—♀♂″™
e͜eő̺Ùo̭⟨⟩ɬƆÐo̱o̮
ȍŒÆȯƏơ·ʁẞÞo̰o̟
o̥ƩʎƷʍ⚠⯑ɧɲǁ̢ǫ

Level5 :
☐ ⃠♥💡⛏„‚▶≺≻ª🛈
o̩ɘo̻ɯʌ⌊⌋ɮɕo̜ɱȝ
ɜʘɑo̤ɚː•ɾỏƿŋȣ
ɐɸƴƹʊˈ⸮ʔɡǂɤꞌ

Level5+MAJ :
☑⛔☠🍀⚔‟‛⧫≼≽º℗
o̯ɵ̚ɰɅ⌈⌉̴ o̹o̼Ȝ
ɞ ɒ o˞ˑ◼ɺſǷŊȢ
ⱯƒƳƸƱˌ‽ʕᵹǃɣꞋ


pavé numérique :
0,123456789+-*/^=
x.ABCDEF()∞±−×÷e≈
b €$¤→⇒⇔ij∀∑∓⋅:⏨≠
∅∈ℙℕℤⅅℚℝℂℍ∃∫∂∘¬√≝



Problèmes constatés :
eia̢n → le palatale ne fait pas l’attendu, il faut faire une touche morte.
au z̡z ̡s ̢s̢z 
̡N aį̥a  ̡Nɔłæ

̵ ne fait pas l’attendu





// ┌─────┲━━━━━┯━━━━━┯━━━━━┯━━━━━┯━━━━━┳━━━━━┯━━━━━┳━━━━━┯━━━━━┯━━━━━┯━━━━━┯━━━━━┳━━━━━━━━━┓  ┏━━━━━┱─────┬─────┬─────┐
// │     ║     │     │     │     │     ║     │     ║     │     │     │     │     ║ ⌫ Retour┃  ┃     ┃     │     │     │
// │     ║     │     │     │     │     ║     │     ║     │     │     │     │     ║  arrière┃  ┃     ┃     │     │     │
// ┢━━━━━┻═┳═━═┷═┯═━═┷═┯═━═┷═┯═━═┷═┯═━═┻═┱───┴─┬───┺═┳═━═┷═┯═━═┷═┯═━═┷═┯═━═┷═┯═━═┻━┳━━━━━━━┫  ┡━━━━━╃─────┼─────┼─────┤
// ┃   ↹   ┃     │     │     │     │     ║     │     ║     │     │     │     │     ┃       ┃  │     │     │     │     │
// ┃  Tab  ┃     │     │     │     │     ║     │     ║     │     │     │     │     ┃Entrée ┃  │     │     │     │     │
// ┣━━━━━━━┻┱────┴┬────┴┬────┴┬────┴┬────┺┱────┴┬────┺┱────┴┬────┴┬────┴┬────┴┬────┺┓      ┃  ├─────┼─────┼─────┤     │
// ┃   ⇬    ┃     │     │     │     │     ║     │     ║     │     │     │     │     ┃  ⏎   ┃  │     │     │     │     │
// ┃ Level5 ┃     │     │     │     │     ║     │     ║     │     │     │     │     ┃      ┃  │     │     │     │     │
// ┣━━━━━━━┳┹────┬┴────┬┴────┬┴────┬┴────┲┹────┬┴────┲┹────┬┴────┬┴────┬┴────┲┷━━━━━┻━━━━━━┫  ├─────┼─────┼─────╆━━━━━┪
// ┃   ⇧   ┃     │     │     │     │     ║     │     ║     │     │     │     ┃     ¦  ⇧    ┃  │     │     │     ┃     ┃
// ┃  MAJ  ┃     │     │     │     │     ║     │     ║     │     │     │     ┃ (ç) ¦ MAJ   ┃  │     │     │     ┃     ┃
// ┣━━━━━┳━┻━━━┳━┷━━━┳━┷━━━┱─┴─────┴─────┺━━━━━┷━━━━━┹─────┴───┲━┷━━━━━╈━━━━━┻━┳━━━━━━━━━━━┫  ├─────┴─────┼─────┨     ┃
// ┃  ⎈  ┃     ┃  ⌘  ┃  ⌥  ┃                                   ┃   ⇮   ┃       ┃     ↑     ┃  │           │     ┃     ┃
// ┃Ctrl ┃ Fn  ┃Meta ┃ Alt ┃                                   ┃ AltGr ┃ Compo ┃  ←  ↓  →  ┃  │           │     ┃     ┃
// ┗━━━━━┻━━━━━┻━━━━━┻━━━━━┹───────────────────────────────────┺━━━━━━━┻━━━━━━━┻━━━━━━━━━━━┛  └───────────┴─────┺━━━━━┛

								$	£	¢							
									₻								
								€	¥	◌¤							
						◌in	⊂	⊆	◌ex	⊃	⊇						
																	
						(	◌̃ 	∈	)	̷	∋						
					F	ℜ	⟘	E	͞◌	∩	D	⊨ 	∧				
					𝒫			℘ 			ℸ						
					6	ℝ	⟙	5	ℚ	∪	4	ⅅ	∨				
			i	ℑ	⊣	π	→	↦	ℯ	⋮	⋰	C	∁	⊲			
			𝒞			ℓ 			ℳ			ℷ					
			7	ℂ	⊢	,	⇒	◌⃗	.	⋯	⋱	3	ℤ	⊳			
		×	∘	∗	j	ℏ	◌⃟	∞	⌀	⬚	B	⃘	◌⃤	±	∫	∭	
		🌐			ℱ			ℒ			ℶ						
		*	⋅	∏	8	ℍ	◌⃞	0	∅	◌	2	ℕ	◌⃝	+	∑	∬ 	
	⧠	÷	≀	∐	k	∃	‖	A	⫽	∢	−	∂	∇	≈	≡	≝
⏦			⎓			𝒵			ℵ	₪					≙		≟
	∛	/	¬	Ш	9	∀	∣	1	ℙ	⟂	-	∓	∆	=	⇔	∝


_____________________________________________________________________________________________________

//**************************************
//*       Bépo Original V1.0rc2        *
//**************************************

// Bépo : Improved ergonomic french keymap using Dvorak method.
// Built by community on 'Dvorak Fr / Bépo' :
// see http://www.clavier-dvorak.org/wiki/ to join and help.
// XOrg integration (1.0rc2 version) in 2008
// by Frédéric Boiteux <fboiteux at free dot fr>
//
// Bépo layout (1.0rc2 version) for a pc105 keyboard (french) :
// ┌─────┐
// │ S A │   S = Shift,  A = AltGr + Shift
// │ s a │   s = normal, a = AltGr
// └─────┘
//
// ┌─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┲━━━━━━━━━┓
// │ # ¶ │ 1 „ │ 2 “ │ 3 ” │ 4 ≤ │ 5 ≥ │ 6   │ 7 ¬ │ 8 ¼ │ 9 ½ │ 0 ¾ │ ° ′ │ ` ″ ┃ ⌫ Retour┃
// │ $ – │ " — │ « < │ » > │ ( [ │ ) ] │ @ ^ │ + ± │ - − │ / ÷ │ * × │ = ≠ │ % ‰ ┃  arrière┃
// ┢━━━━━┷━┱───┴─┬───┴─┬───┴─┬───┴─┬───┴─┬───┴─┬───┴─┬───┴─┬───┴─┬───┴─┬───┴─┬───┺━┳━━━━━━━┫
// ┃       ┃ B ¦ │ É ˝ │ P § │ O Œ │ È ` │ !   │ V   │ D Ð │ L   │ J Ĳ │ Z Ə │ W   ┃Entrée ┃
// ┃Tab ↹  ┃ b | │ é ˊ  │ p & │ o œ │ è ` │ ˆ ¡ │ v ˇ │ d ð │ l / │ j ĳ │ z ə │ w ̆  ┃   ⏎   ┃
// ┣━━━━━━━┻┱────┴┬────┴┬────┴┬────┴┬────┴┬────┴┬────┴┬────┴┬────┴┬────┴┬────┴┬────┺┓      ┃
// ┃        ┃ A Æ │ U Ù │ I ˙ │ E ¤ │ ; ̛ │ C ſ │ T Þ │ S ẞ │ R ™ │ N   │ M º │ Ç , ┃      ┃
// ┃Maj ⇬   ┃ a æ │ u ù │ i ̈  │ e € │ , ’ │ c © │ t þ │ s ß │ r ® │ n ˜ │ m ¯ │ ç ¸ ┃      ┃
// ┣━━━━━━━┳┹────┬┴────┬┴────┬┴────┬┴────┬┴────┬┴────┬┴────┬┴────┬┴────┬┴────┲┷━━━━━┻━━━━━━┫
// ┃       ┃ Ê   │ À   │ Y ‘ │ X ’ │ : · │ K   │ ? ̉  │ Q ̣  │ G   │ H ‡ │ F ª ┃             ┃
// ┃Shift ⇧┃ ê / │ à \ │ y { │ x } │ . … │ k ~ │ ' ¿ │ q ˚ │ g µ │ h † │ f ˛ ┃Shift ⇧      ┃
// ┣━━━━━━━╋━━━━━┷━┳━━━┷━━━┱─┴─────┴─────┴─────┴─────┴─────┴───┲━┷━━━━━╈━━━━━┻━┳━━━━━━━┳━━━┛
// ┃       ┃       ┃       ┃ Espace inséc.   Espace inséc. fin ┃       ┃       ┃       ┃
// ┃Ctrl   ┃Meta   ┃Alt    ┃ ␣ (Espace)      _               ␣ ┃AltGr ⇮┃Menu   ┃Ctrl   ┃
// ┗━━━━━━━┻━━━━━━━┻━━━━━━━┹───────────────────────────────────┺━━━━━━━┻━━━━━━━┻━━━━━━━┛
partial alphanumeric_keys
xkb_symbols "bepo" {

    include "level3(ralt_switch)"
    include "keypad(oss)"

    name[Group1]= "French (Bepo, ergonomic, Dvorak way)";

    // First row
    key <TLDE> { [          dollar,   numbersign,        endash,       paragraph ] }; // $ # – ¶
    key <AE01> { type[group1] = "FOUR_LEVEL_SEMIALPHABETIC", [        quotedbl,            1,         emdash, doublelowquotemark ] }; // " 1 — „
    key <AE02> { type[group1] = "FOUR_LEVEL_SEMIALPHABETIC", [   guillemotleft,            2,           less,  leftdoublequotemark ] }; // « 2 < “
    key <AE03> { type[group1] = "FOUR_LEVEL_SEMIALPHABETIC", [  guillemotright,            3,        greater, rightdoublequotemark ] }; // » 3 > ”
    key <AE04> { type[group1] = "FOUR_LEVEL_SEMIALPHABETIC", [       parenleft,            4,    bracketleft,      lessthanequal ] }; // ( 4 [ ≤
    key <AE05> { type[group1] = "FOUR_LEVEL_SEMIALPHABETIC", [      parenright,            5,   bracketright,   greaterthanequal ] }; // ) 5 ] ≥
    key <AE06> { type[group1] = "FOUR_LEVEL_SEMIALPHABETIC", [              at,            6,    asciicircum                 ] }; // @ 6 ^
    key <AE07> { type[group1] = "FOUR_LEVEL_SEMIALPHABETIC", [            plus,            7,      plusminus,        notsign ] }; // + 7 ± ¬
    key <AE08> { type[group1] = "FOUR_LEVEL_SEMIALPHABETIC", [           minus,            8,          U2212,     onequarter ] }; // - 8 − ¼
    key <AE09> { type[group1] = "FOUR_LEVEL_SEMIALPHABETIC", [           slash,            9,       division,        onehalf ] }; // / 9 ÷ ½
    key <AE10> { type[group1] = "FOUR_LEVEL_SEMIALPHABETIC", [        asterisk,            0,       multiply,  threequarters ] }; // * 0 × ¾
    key <AE11> { [           equal,       degree,       notequal,        minutes ] }; // = ° ≠ ′
    key <AE12> { [         percent,        grave,          U2030,        seconds ] }; // % ` ‰ ″

    // Second row
    key <AD01> { [               b,            B,            bar,      brokenbar ] }; // b B | ¦
    key <AD02> { [          eacute,       Eacute,     dead_acute, dead_doubleacute ] }; // é É ˊ ˝
    key <AD03> { [               p,            P,      ampersand,        section ] }; // p P & §
    key <AD04> { [               o,            O,             oe,             OE ] }; // o O œ Œ
    key <AD05> { [          egrave,       Egrave,     dead_grave,          grave ] }; // è È ` `
    key <AD06> { [ dead_circumflex,       exclam,     exclamdown                 ] }; // ^ ! ¡
    key <AD07> { [               v,            V,     dead_caron                 ] }; // v V ˇ
    key <AD08> { [               d,            D,            eth,            ETH ] }; // d D ð Ð
    key <AD09> { [               l,            L,    dead_stroke                 ] }; // l L /
    key <AD10> { [               j,            J,          U0133,          U0132 ] }; // j J ĳ Ĳ
    key <AD11> { [               z,            Z,          schwa,          SCHWA ] }; // z Z ə Ə
    key <AD12> { [               w,            W,     dead_breve                 ] }; // w W ̆

    // Third row
    key <AC01> { [               a,            A,             ae,             AE ] }; // a A æ Æ
    key <AC02> { [               u,            U,         ugrave,         Ugrave ] }; // u U ù Ù
    key <AC03> { [               i,            I, dead_diaeresis,  dead_abovedot ] }; // i I ̈ ˙
    key <AC04> { [               e,            E,       EuroSign,  dead_currency ] }; // e E € ¤
    key <AC05> { [           comma,    semicolon, rightsinglequotemark, dead_horn ] }; // , ; ’ ̛
    key <AC06> { [               c,            C,      copyright,          U017F ] }; // c C © ſ
    key <AC07> { [               t,            T,          thorn,          THORN ] }; // t T þ Þ
    key <AC08> { [               s,            S,         ssharp,          U1E9E ] }; // s S ß ẞ
    key <AC09> { [               r,            R,     registered,      trademark ] }; // r R ® ™
    key <AC10> { [               n,            N,     dead_tilde                 ] }; // n N ~
    key <AC11> { [               m,            M,    dead_macron,      masculine ] }; // m M ̄ º
    key <BKSL> { [        ccedilla,     Ccedilla,   dead_cedilla, dead_belowcomma ] }; // ç Ç ¸ ,

    // Fourth row
    key <LSGT> { [     ecircumflex,  Ecircumflex,          slash                 ] }; // ê Ê /
    key <AB01> { [          agrave,       Agrave,      backslash                 ] }; // à À \
    key <AB02> { [               y,            Y,      braceleft, leftsinglequotemark  ] }; // y Y { ‘
    key <AB03> { [               x,            X,     braceright, rightsinglequotemark ] }; // x X } ’
    key <AB04> { [          period,        colon,       ellipsis, periodcentered ] }; // . : … ·
    key <AB05> { [               k,            K,     asciitilde                 ] }; // k K ~
    key <AB06> { [      apostrophe,     question,   questiondown,      dead_hook ] }; // ' ? ¿ ̉
    key <AB07> { [               q,            Q, dead_abovering,  dead_belowdot ] }; // q Q ˚ ̣
    key <AB08> { [               g,            G,     dead_greek                 ] }; // g G µ
    key <AB09> { [               h,            H,         dagger,   doubledagger ] }; // h H † ‡
    key <AB10> { [               f,            F,    dead_ogonek,    ordfeminine ] }; // f F ̨ ª

    key <SPCE> { [           space, nobreakspace,     underscore,          U202F ] }; // ␣ (espace insécable) _ (espace insécable fin)
};


