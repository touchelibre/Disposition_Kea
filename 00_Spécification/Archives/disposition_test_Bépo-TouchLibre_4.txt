// ***********************
// *  Képuv_ToucheLibre  *
// ***********************
//
// ToucheLibre Keyboard is truly ergonomic keyboard designed with free philosophy.
// Képuv is a Bépo variant disposition for the ToucheLibre Keyboard.
// Bépo is improved ergonomic french keymap using Dvorak method.
// see http://touchelibre.fr
// Not integrated in XOrg
// Version : V0.β3 in juin 2018
// by Lilian Tribouilloy <lilian.tribouilloy@laposte.net>
//
// Képof_ToucheLibre_K380 for Logitech K380 (80 keys) or other keyboard ISO 88 keys :
// ┌─────┐
// │ S A │   S = Shift,  A = AltGr + Shift
// │ s a │   s = direct, a = AltGr
// └─────┘
//				AltGr2 is not available at the moment. So a lot of key is not available.
//				This default could be corrected by the key « ⎄ compose ». ⎄ need to be activated. 
//				The line E is modified because the ToucheLibre's Numpad do not existe in ISO keyboard.
//				So, 1234567890 are sited in direct way instead of special key.
//				Only key «ç» ( <TLDE> ) is not in same place than on ToucheLibre keyboard.
//              (ç) on right MAJ give a better for key «ç».
//
// ┌─────┲━━━━━┯━━━━━┯━━━━━┯━━━━━┯━━━━━┳━━━━━┯━━━━━┳━━━━━┯━━━━━┯━━━━━┯━━━━━┯━━━━━┳━━━━━━━━━┓
// │ Ç ˛ ║ % ° │ § ` │ € ¤ │ # " │ @ ' ║ ‹ ‘ │ › ’ ║ + † │ - — │ * ∞ │ / \ │ = ~ ║ ⌫ Retour┃
// │ ç ¸ ║ 1 & │ 2 | │ 3 $ │ 4 < │ 5 > ║ « “ │ » ” ║ 6 ± │ 7 − │ 8 × │ 9 ÷ │ 0 ^ ║  arrière┃
// ┢━━━━━┻═┳═━═┷═┯═━═┷═┯═━═┷═┯═━═┷═┯═━═┻═┱───┴─┬───┺═┳═━═┷═┯═━═┷═┯═━═┷═┯═━═┷═┯═━═┻━┳━━━━━━━┫
// ┃   ↹   ┃ K  ͜  │ É ˝ │ P ̺  │ U Ù │ V ̭  ║ [ ⟨ │ ] ⟩ ║ L ɬ │ C Ɔ │ D Ð │ M ̱  │ Z ̮  ┃       ┃
// ┃  Tab  ┃ k ͡  │ é ˊ  │ p ̪  │ u ù │ v ˇ ║ ( { │ ) } ║ l / │ c ɔ │ d ð │ m ¯ │ z ̑  ┃Entrée ┃
// ┣━━━━━━━┻┱────┴┬────┴┬────┴┬────┴┬────┺┱────┴┬────┺┱────┴┬────┴┬────┴┬────┴┬────┺┓      ┃
// ┃   ⇬    ┃ È ̏  │ O Œ │ A Æ │ I ˙ │ E Ə ║ ; ̛  │ : · ║ R ʁ │ S ẞ │ T Þ │ N ̰  │ X ̟  ┃  ⏎   ┃
// ┃ M Lock ┃ è ` │ o œ │ a æ │ i ̈  │ e ə ║ , , │ . … ║ r ɹ │ s ß │ t þ │ n ˜ │ x ̽  ┃      ┃
// ┣━━━━━━━┳┹────┬┴────┬┴────┬┴────┬┴────┲┹────┬┴────┲┹────┬┴────┬┴────┬┴────┲┷━━━━━┻━━━━━━┫
// ┃   ⇧   ┃ À ̥  │ F Ʃ │ Y ʎ │ J Ʒ │ W ʍ ║ ! ̣  │ ? ̉  ║ H ɧ │ G ɲ │ B ǂ │ Q ̢  ┃     ¦  ⇧    ┃
// ┃  MAJ  ┃ à ˚ │ f ʃ │ y ɥ │ j ʒ │ w ̆  ║ ’ ¡ │ ˆ ¿ ║ h ̵  │ g µ │ b ǁ │ q ̡  ┃ (ç) ¦ MAJ   ┃
// ┣━━━━━┳━┻━━━┳━┷━━━┳━┷━━━┱─┴─────┴─────┺━━━━━┷━━━━━┹─────┴───┲━┷━━━━━╈━━━━━┻━┳━━━━━━━━━━━┫
// ┃  ⎈  ┃     ┃  ⌘  ┃  ⌥  ┃   ½⍽  ½ spc inséc.  ⍽ spc inséc.  ┃   ⇮   ┃   ⎄   ┃     ↑     ┃
// ┃Ctrl ┃ Fn  ┃Meta ┃ Alt ┃   ␣   Espace        _ under score ┃ AltGr ┃ Compo ┃  ←  ↓  →  ┃
// ┗━━━━━┻━━━━━┻━━━━━┻━━━━━┹───────────────────────────────────┺━━━━━━━┻━━━━━━━┻━━━━━━━━━━━┛
//
// ***********************
//
partial alphanumeric_keys
xkb_symbols "bepo" {

    include "level3(ralt_switch)"
    include "keypad(oss)"

    name[Group1]= "French (Bepo, ergonomic, Dvorak way)";

    // First row
    key <TLDE> { [        ccedilla,       Ccedilla,       dead_cedilla,      dead_ogonek  ] }; // ç Ç ¸ ˛
    key <AE01> { [               1,        percent,          ampersand,           degree  ] }; // 1 % & °
    key <AE02> { [               2,        section,                bar,            grave  ] }; // 2 § | `
    key <AE03> { [               3,       EuroSign,             dollar,    dead_currency  ] }; // 3 € $ ¤
    key <AE04> { [               4,     numbersign,               less,         quotedbl  ] }; // 4 # < "
    key <AE05> { [               5,             at,            greater,       apostrophe  ] }; // 5 @ > '
    key <AE06> { [   guillemotleft,   U2039,   leftdoublequotemark,  leftsinglequotemark  ] }; // « ‹ “ ‘
    key <AE07> { [  guillemotright,   U203A,  rightdoublequotemark, rightsinglequotemark  ] }; // » › ” ’
    key <AE08> { [               6,           plus,          plusminus,           dagger  ] }; // 6 + ± †
    key <AE09> { [               7,          minus,              U2212,           emdash  ] }; // 7 - − —
    key <AE10> { [               8,       asterisk,           multiply,            U221E  ] }; // 8 * × ∞
    key <AE11> { [               9,          slash,           division,        backslash  ] }; // 9 / ÷ \
    key <AE12> { [               0,          equal,        asciicircum,       asciitilde  ] }; // 0 = ^ ~

    // Second row
    key <AD01> { [               k,              K,              U0361,            U035C  ] }; // k K ͡   ͜ 
    key <AD02> { [          eacute,         Eacute,         dead_acute, dead_doubleacute  ] }; // é É ˊ ˝
    key <AD03> { [               p,              P,              U032A,            U033A  ] }; // p P ̪  ̺
    key <AD04> { [               u,              U,             ugrave,           Ugrave  ] }; // u U ù Ù
    key <AD05> { [               v,              V,         dead_caron,            U032D  ] }; // v V ˇ ̭
    key <AD06> { [       parenleft,    bracketleft,          braceleft,            U27E8  ] }; // ( [ { ⟨
    key <AD07> { [      parenright,   bracketright,         braceright,            U27E9  ] }; // ) ] } ⟩
    key <AD08> { [               l,              L,        dead_stroke,            U026C  ] }; // l L / ɬ
    key <AD09> { [               c,              C,              U0254,            U0186  ] }; // c C ɔ Ɔ
    key <AD10> { [               d,              D,                eth,              ETH  ] }; // d D ð Ð
    key <AD11> { [               m,              M,        dead_macron,            U0331  ] }; // m M ̄  ̱
    key <AD12> { [               z,              Z,              U0311,            U032E  ] }; // z Z ̑  ̮ 

    // Third row
    key <AC01> { [          egrave,         Egrave,         dead_grave,            U030F  ] }; // è È ` ̏
    key <AC02> { [               o,              O,                 oe,               OE  ] }; // o O œ Œ
    key <AC03> { [               a,              A,                 ae,               AE  ] }; // a A æ Æ
    key <AC04> { [               i,              I,     dead_diaeresis,    dead_abovedot  ] }; // i I ̈ ˙
    key <AC05> { [               e,              E,              schwa,            SCHWA  ] }; // e E ə Ə
    key <AC06> { [           comma,      semicolon,    dead_belowcomma,        dead_horn  ] }; // , ; , ̛
    key <AC07> { [          period,          colon,           ellipsis,   periodcentered  ] }; // . : … ·
    key <AC08> { [               r,              R,              U0279,            U0281  ] }; // r R ɹ ʁ
    key <AC09> { [               s,              S,             ssharp,            U1E9E  ] }; // s S ß ẞ
    key <AC10> { [               t,              T,              thorn,            THORN  ] }; // t T þ Þ
    key <AC11> { [               n,              N,         dead_tilde,            U0330  ] }; // n N ̃  ̰
    key <BKSL> { [               x,              X,              U033D,            U031F  ] }; // x X ̽  ̟

    // Fourth row
    key <LSGT> { [          agrave,         Agrave,     dead_abovering,            U0325  ] }; // à À ˚ ̥
    key <AB01> { [               f,              F,              U0283,            U01A9  ] }; // f F ʃ Ʃ
    key <AB02> { [               y,              Y,              U0265,            U028E  ] }; // y Y ɥ ʎ
    key <AB03> { [               j,              J,              U0292,            U01B7  ] }; // j J ʒ Ʒ
    key <AB04> { [               w,              W,         dead_breve,            U028D  ] }; // w W ̆  ʍ
    key <AB05> { [ rightsinglequotemark,    exclam,         exclamdown,    dead_belowdot  ] }; // ’ ! ¡ ̣
    key <AB06> { [ dead_circumflex,       question,       questiondown,        dead_hook  ] }; // ^ ? ¿ ̉
    key <AB07> { [               h,              H,              U0335,            U0267  ] }; // h H ̵  ɧ
    key <AB08> { [               g,              G,         dead_greek,            U0272  ] }; // g G µ ɲ
    key <AB09> { [               b,              B,              U01C1,            U01C2  ] }; // b B ǁ ǂ
    key <AB10> { [               q,              Q,              U0321,            U0322  ] }; // q Q ̡  ̢

    // Fifth row
    key <SPCE> { [           space,          U202F,         underscore,     nobreakspace  ] }; // ␣ ½⍽ _ ⍽
};
//
// ***********************
//

*************************************
test / réserve / modif

12345«»67890
képof()lcmdz
èuaie,.rsntx
àvyjw’^qghbç

%§€#@‹›+-*/=
KÉPOF[]LCMDZ
ÈUAIE;:RSNTX
ÀVYJW!?QGHBÇ

&|$<>“”±−×÷^
͡´̪ œʃ{}/ɔ¯ð̑
`ùæ¨ə,…ɹßñþe̽
°ˇɥʒ˘¡¿̡ µ̵ ǁ¸

°`¤"'‘’†—∞\~
͜˝̺ ŒƩ⟨⟩ɬƆe̱Ðe̮
ƩÙÆ˙Ə̛ ·ʁẞ̰ Þe̟
e̥ḙʎƷʍẹ̉ ̢ ɲɧǂ˛

eia̢n → le palatale ne fait pas l’attendu, il faut faire une touche morte.
au z̡z ̡s ̢s̢z 
̡N aį̥a  ̡Nɔłæ


//**************************************
//*       Bépo Original V1.0rc2        *
//**************************************

// Bépo : Improved ergonomic french keymap using Dvorak method.
// Built by community on 'Dvorak Fr / Bépo' :
// see http://www.clavier-dvorak.org/wiki/ to join and help.
// XOrg integration (1.0rc2 version) in 2008
// by Frédéric Boiteux <fboiteux at free dot fr>
//
// Bépo layout (1.0rc2 version) for a pc105 keyboard (french) :
// ┌─────┐
// │ S A │   S = Shift,  A = AltGr + Shift
// │ s a │   s = normal, a = AltGr
// └─────┘
//
// ┌─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┲━━━━━━━━━┓
// │ # ¶ │ 1 „ │ 2 “ │ 3 ” │ 4 ≤ │ 5 ≥ │ 6   │ 7 ¬ │ 8 ¼ │ 9 ½ │ 0 ¾ │ ° ′ │ ` ″ ┃ ⌫ Retour┃
// │ $ – │ " — │ « < │ » > │ ( [ │ ) ] │ @ ^ │ + ± │ - − │ / ÷ │ * × │ = ≠ │ % ‰ ┃  arrière┃
// ┢━━━━━┷━┱───┴─┬───┴─┬───┴─┬───┴─┬───┴─┬───┴─┬───┴─┬───┴─┬───┴─┬───┴─┬───┴─┬───┺━┳━━━━━━━┫
// ┃       ┃ B ¦ │ É ˝ │ P § │ O Œ │ È ` │ !   │ V   │ D Ð │ L   │ J Ĳ │ Z Ə │ W   ┃Entrée ┃
// ┃Tab ↹  ┃ b | │ é ˊ  │ p & │ o œ │ è ` │ ˆ ¡ │ v ˇ │ d ð │ l / │ j ĳ │ z ə │ w ̆  ┃   ⏎   ┃
// ┣━━━━━━━┻┱────┴┬────┴┬────┴┬────┴┬────┴┬────┴┬────┴┬────┴┬────┴┬────┴┬────┴┬────┺┓      ┃
// ┃        ┃ A Æ │ U Ù │ I ˙ │ E ¤ │ ; ̛  │ C ſ │ T Þ │ S ẞ │ R ™ │ N   │ M º │ Ç , ┃      ┃
// ┃Maj ⇬   ┃ a æ │ u ù │ i ̈  │ e € │ , ’ │ c © │ t þ │ s ß │ r ® │ n ˜ │ m ¯ │ ç ¸ ┃      ┃
// ┣━━━━━━━┳┹────┬┴────┬┴────┬┴────┬┴────┬┴────┬┴────┬┴────┬┴────┬┴────┬┴────┲┷━━━━━┻━━━━━━┫
// ┃       ┃ Ê   │ À   │ Y ‘ │ X ’ │ : · │ K   │ ? ̉  │ Q ̣  │ G   │ H ‡ │ F ª ┃             ┃
// ┃Shift ⇧┃ ê / │ à \ │ y { │ x } │ . … │ k ~ │ ' ¿ │ q ˚ │ g µ │ h † │ f ˛ ┃Shift ⇧      ┃
// ┣━━━━━━━╋━━━━━┷━┳━━━┷━━━┱─┴─────┴─────┴─────┴─────┴─────┴───┲━┷━━━━━╈━━━━━┻━┳━━━━━━━┳━━━┛
// ┃       ┃       ┃       ┃ Espace inséc.   Espace inséc. fin ┃       ┃       ┃       ┃
// ┃Ctrl   ┃Meta   ┃Alt    ┃ ␣ (Espace)      _               ␣ ┃AltGr ⇮┃Menu   ┃Ctrl   ┃
// ┗━━━━━━━┻━━━━━━━┻━━━━━━━┹───────────────────────────────────┺━━━━━━━┻━━━━━━━┻━━━━━━━┛
partial alphanumeric_keys
xkb_symbols "bepo" {

    include "level3(ralt_switch)"
    include "keypad(oss)"

    name[Group1]= "French (Bepo, ergonomic, Dvorak way)";

    // First row
    key <TLDE> { [          dollar,   numbersign,        endash,       paragraph ] }; // $ # – ¶
    key <AE01> { type[group1] = "FOUR_LEVEL_SEMIALPHABETIC", [        quotedbl,            1,         emdash, doublelowquotemark ] }; // " 1 — „
    key <AE02> { type[group1] = "FOUR_LEVEL_SEMIALPHABETIC", [   guillemotleft,            2,           less,  leftdoublequotemark ] }; // « 2 < “
    key <AE03> { type[group1] = "FOUR_LEVEL_SEMIALPHABETIC", [  guillemotright,            3,        greater, rightdoublequotemark ] }; // » 3 > ”
    key <AE04> { type[group1] = "FOUR_LEVEL_SEMIALPHABETIC", [       parenleft,            4,    bracketleft,      lessthanequal ] }; // ( 4 [ ≤
    key <AE05> { type[group1] = "FOUR_LEVEL_SEMIALPHABETIC", [      parenright,            5,   bracketright,   greaterthanequal ] }; // ) 5 ] ≥
    key <AE06> { type[group1] = "FOUR_LEVEL_SEMIALPHABETIC", [              at,            6,    asciicircum                 ] }; // @ 6 ^
    key <AE07> { type[group1] = "FOUR_LEVEL_SEMIALPHABETIC", [            plus,            7,      plusminus,        notsign ] }; // + 7 ± ¬
    key <AE08> { type[group1] = "FOUR_LEVEL_SEMIALPHABETIC", [           minus,            8,          U2212,     onequarter ] }; // - 8 − ¼
    key <AE09> { type[group1] = "FOUR_LEVEL_SEMIALPHABETIC", [           slash,            9,       division,        onehalf ] }; // / 9 ÷ ½
    key <AE10> { type[group1] = "FOUR_LEVEL_SEMIALPHABETIC", [        asterisk,            0,       multiply,  threequarters ] }; // * 0 × ¾
    key <AE11> { [           equal,       degree,       notequal,        minutes ] }; // = ° ≠ ′
    key <AE12> { [         percent,        grave,          U2030,        seconds ] }; // % ` ‰ ″

    // Second row
    key <AD01> { [               b,            B,            bar,      brokenbar ] }; // b B | ¦
    key <AD02> { [          eacute,       Eacute,     dead_acute, dead_doubleacute ] }; // é É ˊ ˝
    key <AD03> { [               p,            P,      ampersand,        section ] }; // p P & §
    key <AD04> { [               o,            O,             oe,             OE ] }; // o O œ Œ
    key <AD05> { [          egrave,       Egrave,     dead_grave,          grave ] }; // è È ` `
    key <AD06> { [ dead_circumflex,       exclam,     exclamdown                 ] }; // ^ ! ¡
    key <AD07> { [               v,            V,     dead_caron                 ] }; // v V ˇ
    key <AD08> { [               d,            D,            eth,            ETH ] }; // d D ð Ð
    key <AD09> { [               l,            L,    dead_stroke                 ] }; // l L /
    key <AD10> { [               j,            J,          U0133,          U0132 ] }; // j J ĳ Ĳ
    key <AD11> { [               z,            Z,          schwa,          SCHWA ] }; // z Z ə Ə
    key <AD12> { [               w,            W,     dead_breve                 ] }; // w W ̆

    // Third row
    key <AC01> { [               a,            A,             ae,             AE ] }; // a A æ Æ
    key <AC02> { [               u,            U,         ugrave,         Ugrave ] }; // u U ù Ù
    key <AC03> { [               i,            I, dead_diaeresis,  dead_abovedot ] }; // i I ̈ ˙
    key <AC04> { [               e,            E,       EuroSign,  dead_currency ] }; // e E € ¤
    key <AC05> { [           comma,    semicolon, rightsinglequotemark, dead_horn ] }; // , ; ’ ̛
    key <AC06> { [               c,            C,      copyright,          U017F ] }; // c C © ſ
    key <AC07> { [               t,            T,          thorn,          THORN ] }; // t T þ Þ
    key <AC08> { [               s,            S,         ssharp,          U1E9E ] }; // s S ß ẞ
    key <AC09> { [               r,            R,     registered,      trademark ] }; // r R ® ™
    key <AC10> { [               n,            N,     dead_tilde                 ] }; // n N ~
    key <AC11> { [               m,            M,    dead_macron,      masculine ] }; // m M ̄ º
    key <BKSL> { [        ccedilla,     Ccedilla,   dead_cedilla, dead_belowcomma ] }; // ç Ç ¸ ,

    // Fourth row
    key <LSGT> { [     ecircumflex,  Ecircumflex,          slash                 ] }; // ê Ê /
    key <AB01> { [          agrave,       Agrave,      backslash                 ] }; // à À \
    key <AB02> { [               y,            Y,      braceleft, leftsinglequotemark  ] }; // y Y { ‘
    key <AB03> { [               x,            X,     braceright, rightsinglequotemark ] }; // x X } ’
    key <AB04> { [          period,        colon,       ellipsis, periodcentered ] }; // . : … ·
    key <AB05> { [               k,            K,     asciitilde                 ] }; // k K ~
    key <AB06> { [      apostrophe,     question,   questiondown,      dead_hook ] }; // ' ? ¿ ̉
    key <AB07> { [               q,            Q, dead_abovering,  dead_belowdot ] }; // q Q ˚ ̣
    key <AB08> { [               g,            G,     dead_greek                 ] }; // g G µ
    key <AB09> { [               h,            H,         dagger,   doubledagger ] }; // h H † ‡
    key <AB10> { [               f,            F,    dead_ogonek,    ordfeminine ] }; // f F ̨ ª

    key <SPCE> { [           space, nobreakspace,     underscore,          U202F ] }; // ␣ (espace insécable) _ (espace insécable fin)
};

