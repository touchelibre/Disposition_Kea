> Par Lilian Tribouilloy ; Le 29 avril 2018

*****************************************************
# Parties intéressantes (selon moi) dans l'unicode  #
*****************************************************

Le but est de sélectionné les caractères à placer sur le clavier ToucheLibre. Soit directement. Soit indirectement par l'utilisation d'une touche modificatrice (Maj+, AltGr+...). Soit par l'utilisation de la touche «compose».

Dans un premier temps je fais une présélection dans l'Unicode selon plusieurs critères majeurs et mineurs :

Critères Majeur ⇒ À intégrer absolument à la disposition Lily‑Bépo avec un accès facilité (facilement mémorisable)

1. Alphabet Latin, Grec ( → les seules alphabet que j'utilise vraiment. Le latin au quotidien et le grec pour les sciences et la technologie)
2. Symboles scientifiques et techniques courants ( → enseigné à l'école et faisant partie de l'ISO 80 000 décrivant les conventions scientifiques et technologiques)
3. Les logos et les émoticônes que j'aime et que j'utilise souvent. ( → C'est subjectif évidement)


Critères Mineur ⇒ À mettre dans «autre clavier»  et/ou accessible avec la touche compose.

4. Alphabet Cyrillique, Arabe, Heubreu ( → pour attirer les utilisateurs "non latin". A noter que les chinois utilent l'alphabet latin pour l'écriture PingYin pour faciliter l'usage de l'informatique. Donc l'intégration des idéogrammes CJC ne semble pas nécessaire.)
5. Symboles scientifiques et techniques rares
6. Autres logo et émoticône
 
Sinon, ce n'est pas pris en compte par le clavier ToucheLibre. C'est le cas de la plupart des langues vivantes qui n'utilise pas l'alphabet latin ainsi que les langues mortes. 


Autre objectif : identifier les polices qui savent prendre en compte tous les éléments de la sélection majeure et mineure
________________________________________

## Première passe

L'Unicode est divisé en 17 plans (numéroté de 0 à 16) comprenant chacun 16^4 = 65 536 caractères possibles. Soit 1 114 112 caractères possibles au total. Toutes les cases ne sont pas encore attribuées. L'Unicode évolue régulièrement depuis 1991.

Le plan 2 est utilisé pour les idéogrammes complémentaires chinois. Les plans 3 à 13 ne sont pas attribué aujourd'hui. Les plans 14 à 16 ne présente pas d'intérêt pour le commun des mortels.

Reste les plans 0 et 1 à explorer en détail.


## Sélection MAJEUR

Plan 0 : 
0000-077F				(1920)
1D00-1FFF				(768)
2000-2BFF + 2C60-2C7F	(3104)

Plan 1 :
1D400-1D7FF 	(1024) ⇒ À voir ? (formalisme gras, italique...)
1F000-1F7FF 	(2048) ⇒ Choisir les icônes à placer en majeur ou mineur

*(Soit jusqu'à 8864 caractères)* 

## sélection mineur

Plan 0 :
2DE0-2E7F			(160)
A720-A7FF			(224)
FB00-FFFF			(1280)

Plan 1 :
1F000-1F7FF 		(2048) ⇒ Choisir les icônes à placer en majeur ou mineur

*(Soit jusqu'à 3712 caractères)* 
____________________________________________

## Sélection effective

Voir fichier: *Disposition_ToucheLibre_Vx.x.ods*

La présélection donne jusqu'à 12 576 caractères (soit 9,6% des plans 0 et 1).

Le clavier ToucheLibre compte 48 touches des textes + 2 touches (redondance) pour l'espace + 21 touches de calcul (pavé numérique) + 4 touches de flèche + 12 touches de fonctions + 18 touches de contrôle (redondance gauche/droite) + 6 touches modificatrices (redondance gauche/droite). Sans compter les parties sourie et écran tactile.

Donc, 74 touches utilisables pour générer des caractères (texte + espace + calcul + flèche). Avec 3 touches modificatrices (Maj, AltGr1(super) et AltGr2(hyper)), ça fait 444 caractères sélectionnables facilement. On peut ajouter le système des touches mortes pour le grec et les symboles monétaires et les signes diacritiques. Qui donnent 2 x 30 x 2 = 120 caractères supplémentaires possibles.

Un total de 342 caractères (4,45% de la présélection) disposeront soit d'un accès direct soit d'un raccourcis par touche modificatrice ou par touche morte. Cette portion devra représenter au moins 95% de l'occurrence d'usage. Les 12016 autres caractères sélectionnés seront éventuellement gérer par la touche «compose».

Remarque : Les touches mortes pour les signes diacritiques ou aussi appelés signes diacritiques sans chasses. ne sont pas compter ici bien qu'ils apportent aussi leur lot de raccourcis.

____________________________________________

## Police de caractère prennant en compte la sélection

Candidats à vérifier chez GNU/Linux :
* Symbola
*

Candidats à vérifier chez Windows
* segeo UI symbol
*

