[Site Web du Projet](http://touchelibre.fr)

# Disposition Kéa

![Logo Projet](00_Spécification/Logo_Kéa/Logo_ToucheLibre_V3.png)


## Présentation du Projet

![Logo Kéa](00_Spécification/Logo_Kéa/Logo_Kéa.png)

![Photo du Nano PC](00_Spécification/images/Disposition_Kéa.png)

Le projet Kéa a pour but de donner une disposition par défaut au clavier TouLi.

Kéa est basé sur la méthode Dvorak et inspiré du Bépo.

Pour obtenir une solution qui soit réellement ergonomique, une bonne géométrie ne suffit pas, il faut aussi choisir  une bonne disposition de touche. C’est précisément le rôle du Kéa.

À ce titre le Bépo, peut également être un bon choix. Le Kéa, se distingue du Bépo par le fait d’être dès le départ conçu pour un clavier dont la géométrie est ergonomique. Alors que le Bépo à vocation à s’insérer dans l’écosystème  normé existant avec les géométries standards existantes tels que ISO (européen), ANSI (américain) ou JIS (japonais).

Ainsi, en un sens le Kéa est un fork du Bépo pour d’une part s’adapter aux nouvelles possibilités de la géométrie ToucheLibre et d’autre part pour offrir des possibilités nouvelles avec des caractéristiques inédites.

Les paradigmes de la disposition Kéa

Les choix initiaux pour construire cette disposition sont :

* Optimiser à la fois pour le français, l’anglais et l’espagnol.
* Pouvoir écrire toutes les langues basé sur l’alphabet latin en ayant en accès tous les types d’accent imaginables.
* De faciliter la programmation en ayant tous les caractères ASCII en accès facile.
* Accéder aux symboles mathématiques et techniques courants.
* Accéder à l’alphabet grec par une touche (dite) morte. C’est fort utile en science.
* De plus, on limite le mouvement latéral du petit doigt.
* Et accéder à l’alphabet phonétique pour faciliter l’apprentissage des langues.
* Faciliter la mémorisation du clavier en plaçant l’une à côté de l’autre, les touches proches par la forme, par la phonétique ou par leur origine historique (autant que possible en tout cas).
* Enfin, le Kéa proposera une extension par proximité phonétique vers les autres alphabet que le latin : Grec, Cyrillique, Arabe, Hébreu, Bopomofo (alphabet phonétique pour le Chinois) et les Kanas japonais.



## Avancement

Voici le reste à faire et l’intention final du projet. L’avancement est donné entre accolade {x %}.

- [x] {100%} Spécification générale
- [ ] {90%} Pilote pour Linux
- [ ] {80%} Pilote pour Window
- [ ] {80%} Variante pour clavier ISO


## Les dossiers

Le dossier **00_Spécification** permet de définir la disposition souhaitée.

Le dossier **01_Justification** permet de donner une justification théorique sur l’optimisation de la distance parcourue par les doigts.

Le dossier **02_Logiciel** donne l’implémentation effective de la disposition.


## Licence

La licence attribuée est différente selon le métier considéré. Aussi, il y a une licence différente dans chaque dossier selon la répartition suivante :

| Dossier          | Licence (SPDX identifier) |
| ---------------- | ------------------------- |
| 00_Spécification | CC-BY-SA-4.0-or-later     |
| 01_Justification | CC-BY-SA-4.0-or-later     |
| 02_Logiciel      | GNU GPL-3.0-or-later      |

Voir <https://spdx.org/licenses/>


## À Propos de l’Auteur

__Lilian Tribouilloy :__

* __Formation :__ Ingénieur en électronique, diplômé de l’[ENSEA](https://www.ensea.fr/fr) en 2004.
* __Métier :__ Concepteur électronique spécialisé dans les radiofréquences et la CEM (Compatibilité ÉlectroMagnétique).
* __Exemples de produit conçu dans le cadre professionnel :__ Émetteur et Réémetteur pour la télévision numérique ; Amplificateur de puissance classe AB pour une modulation OFDM ; Calculateur entrées/sorties pour camion ; Tableau de bord pour véhicule spéciaux ; Boîtier de télématique pour la gestion de flotte.
* __Objet Libre conçu :__ [ToucheLibre](http://touchelibre.fr/), un clavier d’ordinateur ergonomique en bois. Alliant l’esthétique à l’utile, il s’inscrit dans des valeurs de liberté, d’écologie et de santé.


