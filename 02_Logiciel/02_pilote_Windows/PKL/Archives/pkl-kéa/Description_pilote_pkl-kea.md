## Le Pilote pkl-KÉA

Le pilote pkl-kea.exe pour Windows donne un accès partiel à la disposition KÉA en raison de ses limitations en thermes d’encodage.

La recherche d’un meilleur pilote est en cours.

Toutfois le principale est fonctionnel, et offre un bonne apperçu des avantages de la disposition Kéa.

Le pilote pour Linux est lui beaucoup plus complet.

Deux version de disposition sont disponibles : «clavier ISO105» ; «clavier ISO88»
La version «clavier ISO105» est fortement recommandée sauf si vous n’avez pas de pavé numérique sur votre clavier.


## Gliphes disponibles sur la version «clavier ISO105»

En direct :
ç§/$@"«»-<>%=		// Dans le Kéa final le «-» sera insécable. Le glyphe = ne correspondent pas à ce qu’on veut au final.
 képuv()lcdmz
 èoaie,.rstnx
 àfyjw’^hgbq		// Remarque : le «ç» sera après le «q» dans la géométrie ToucheLibre.

Avec Majuscule :
Ç~\&#'‹›¬©®°^		// Les glyphes ¬©®^ ne correspondent pas à ce qu’on veut au final.
 KÉPUV[]LCDMZ
 ÈOAIE;:RSTNX
 ÀFYJW!?HGBQ

Avec AltGr :
¸¶|  †“”–    		// Il manque des glyphes à la place des « ».
  ´ ùˇ{}/ ð¯ 		// Il manque des glyphes à la place des « ».
 `œæ¨€,… ßþ~ 		// Il manque des glyphes à la place des « ». Le glyphe € ne correspondent pas à ce qu’on veut au final.
 °   ˘¡¿ µ  		// Il manque des glyphes à la place des « ».

Avec AltGr + Maj :
˛¤¦  ‡‘’—   ™		// Il manque des glyphes à la place des « ». Le glyphe ¤ ne correspondent pas à ce qu’on veut au final
  ˝ Ù     Ð  		// Il manque des glyphes à la place des « ».
  ŒÆ ¤̛· ßÞ  		// Il manque des glyphes à la place des « ». Le glyphe ¤ ne correspondent pas à ce qu’on veut au final.
      ̣ ̉     		// Il manque des glyphes à la place des « ».


## Correspondance avec une disposition Azerty

En direct :
²&é"'(-è_çà)=
 azertyuiop^$
 qsdfghjklmù*
 <wxcvbn,;:!

> Remarque : Le pavé numérique n’est pas modifié.


