#!/bin/sh -x
#
# passage en clavier fr-dvorak-tete2beug
#
# Copyright (C) 2017 Lilian Tribouilloy <lilian.tribouilloy@laposte.net>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#

set -ev

# quel répertoire temporaire utiliser ?
if [ -n "$TMP" ]; then
  tmp="$TMP"
elif [ -n "$TMPDIR" ]; then
  tmp="$TMPDIR"
else
  tmp=/tmp
fi
dump="$tmp/$USER-dump.xkb"
dir=`dirname $0`

# quel logiciel de dialogue ?
dialog=echo
which xmessage && dialog="xmessage -button OK:0,Retour:1"
which kdialog && dialog="kdialog --title fr-dvorak-tête2beug --warningcontinuecancel"
which zenity && dialog="zenity --question --title fr-dvorak-tête2beug --text"

# dump la config actuelle si elle n'existe pas, pour être sur de ne pas faire
# une sauvegarde de notre config si l'utilisateur a relancé ce script alors
# qu'il était déjà en tête2beug.
if [ ! -e "$dump" ]; then
  xkbcomp $DISPLAY -xkb "$dump"
fi
# et utilise la notre à la place
xkbcomp -w0 "$dir/fr-dvorak-tete2beug.xkb" $DISPLAY || xkbcomp -w0 "$dir/fr-dvorak-tete2beug-xorglegacy.xkb" $DISPLAY


if `$dialog "Vous utilisez maintenant la disposition de clavier fr-dvorak-tête2beug."` ; then
  echo "Utilisez 'clavier_precedent' pour restaurer la configuration de clavier."
else
  # restaurons le clavier...
  dir=`dirname $0`
  $dir/clavier_precedent
fi
