/**************************************************************************************************/
/*                               AUTOFORMATION au LANGAGE C                                       */
/**************************************************************************************************/
/*   à partir du livre : Le Langage C (norme ANSI) ; auissi connu sous le nom : K&R               */
/*   Auteurs : Brian W. KERNIGHAN ; Dennis M. RITCHIE                                             */
/*   Traduction Française : Jean‑François Groff ; Éric Mottir ; Étienne Alard                     */
/*   édité par : Prentice Hall puis Dunod ; 2ième édition ; 2014                                  */
/*                                                                                                */
/*   Exercice : §1-14 ; page 23                                                                   */
/*   réalisé par : Lilian Tribouilloy                                                             */
/*   Date : 5 septembre 2020                                                                      */
/*                                                                                                */
/*   License : Libre de droit                                                                     */
/**************************************************************************************************/

/*---------------------------------Description du Programme---------------------------------------*/
//
// Création d’un corpus statistique pour l’analyse fréquencielle des glyphes utilisés.
// Saisir un texte, et compter les occurences des caractères.
//
//Liste des caractéres pris en compte dans ce programme.
//	1/ Caractère ASCII imprimable:
// !"#$%&'()*+,-./
//0123456789:;<=>?
//@ABCDEFGHIJKLMNO
//PQRSTUVWXYZ[\]^_
//`abcdefghijflmno
//pqrstuvwxyz{|}~
//
//	2/ Caractère du Complément latin-1:
// ¡¢£¤¥¦§¨©ª«¬­ ®¯
//°±²³´µ¶·¸¹º»¼½¾¿
//ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏ
//ÐÑÒÓÔÕÖ×ØÙÚÛÜÝÞß
//àáâãäåæçèéêëìíîï
//ðñòóôõö÷øùúûüýþÿ


/*---------------------------------Déclarations Préalables----------------------------------------*/

/*Appel des bibliothèques standards*/
#include <stdio.h>


/*Appel des bibliothèques spécifiques au projet*/
/*néant*/


/*Définition des constantes absolues*/
#define Nmax 100		//nombre maximum de caractère analysé

#define Ascii1 " !\"#$%&'()*+,-./"
#define Ascii2 "0123456789:;<=>?"
#define Ascii3 "@ABCDEFGHIJKLMNO"
#define Ascii4 "PQRSTUVWXYZ[\\]^_"
#define Ascii5 "`abcdefghijklmno"
#define Ascii6 "pqrstuvwxyz{|}~"


/*Définition des structures*/
/*néant*/


/*---------------------------------Définition des Fonctions---------------------------------------*/



/* --------------------------------Execution du Main--------------------------------------------- */

int main(void)
{
	/*Déclarations*/
	int c;				//caractère lu
	int NbCa[128];		//Nombre d’occurence par caractère
	int n = 0;			//compteur et indice de mot
	int nhascii = 0;	//nombre de caractère hors ascii
	
	/*Instructions*/
	puts("Mettre fin à l’exercice en tapant sur «@».");
	puts("Attention : les caractères hors ascii peuvent compter ×2, ×3 ou ×4.");
	puts("\n   -      ×    ++  ***  ++    ×      -    \n");
	
	/*		Initialisation du compteur d’occurence*/
	for(int i = 0 ; i <= 128 ; i++)
		NbCa[i] = 0;
	
	
	/*		Comptage des longueurs*/
	c = getchar();
	
	while(c != '@')		//@ car, je ne peux pas générer EOF sur le terminal.
	{
		n++;
		
		if((c > 31) && (c < 127))
			NbCa[c]++;
		
		if (c>=127)
			nhascii++;
		
		c = getchar();
	}
	
	/*		Affichage du barregraphe*/
	printf("********************************************\n\n");
	
	printf("Occurence des %d glyphes saisis:\n\n", n);
	
	for(int i = 32 ; i < 127 ; i++)
	{
		printf("%c = %d\n", i, NbCa[i]);
	}
	printf("caractère hors ascii = %d", nhascii);
	
	/*fin programme*/
	return 0;		//pas d’erreur
}

/**************************************************************************************************/
