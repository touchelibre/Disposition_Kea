/**************************************************************************************************/
/*                               AUTOFORMATION au LANGAGE C                                       */
/**************************************************************************************************/
/*   à partir du livre : Le Langage C (norme ANSI) ; auissi connu sous le nom : K&R               */
/*   Auteurs : Brian W. KERNIGHAN ; Dennis M. RITCHIE                                             */
/*   Traduction Française : Jean‑François Groff ; Éric Mottir ; Étienne Alard                     */
/*   édité par : Prentice Hall puis Dunod ; 2ième édition ; 2014                                  */
/*                                                                                                */
/*   Exercice : §1-14 ; page 23                                                                   */
/*   réalisé par : Lilian Tribouilloy                                                             */
/*   Date : 5 septembre 2020                                                                      */
/*                                                                                                */
/*   License : Libre de droit                                                                     */
/**************************************************************************************************/

/*---------------------------------Description du Programme---------------------------------------*/
//
// Création d’un corpus statistique pour l’analyse fréquencielle des glyphes utilisés.
// Saisir un texte, et compter les occurences des caractères.
//
//Liste des caractéres pris en compte dans ce programme.
//	1/ Caractère ASCII imprimable:
// !"#$%&'()*+,-./
//0123456789:;<=>?
//@ABCDEFGHIJKLMNO
//PQRSTUVWXYZ[\]^_
//`abcdefghijflmno
//pqrstuvwxyz{|}~
//
//	2/ Caractère du Complément latin-1:
// ¡¢£¤¥¦§¨©ª«¬­ ®¯
//°±²³´µ¶·¸¹º»¼½¾¿
//ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏ
//ÐÑÒÓÔÕÖ×ØÙÚÛÜÝÞß
//àáâãäåæçèéêëìíîï
//ðñòóôõö÷øùúûüýþÿ


/*---------------------------------Déclarations Préalables----------------------------------------*/

/*Appel des bibliothèques standards*/
#include <stdio.h>


/*Appel des bibliothèques spécifiques au projet*/
/*néant*/


/*Définition des constantes absolues*/
#define Nmax 100		//nombre maximum de caractère analysé

#define Ascii1 " !\"#$%&'()*+,-./"
#define Ascii2 "0123456789:;<=>?"
#define Ascii3 "@ABCDEFGHIJKLMNO"
#define Ascii4 "PQRSTUVWXYZ[\\]^_"
#define Ascii5 "`abcdefghijflmno"
#define Ascii6 "pqrstuvwxyz{|}~"

#define Latin1 " ¡¢£¤¥¦§¨©ª«¬­ ®¯"
#define Latin2 "°±²³´µ¶·¸¹º»¼½¾¿"
#define Latin3 "ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏ"
#define Latin4 "ÐÑÒÓÔÕÖ×ØÙÚÛÜÝÞß"
#define Latin5 "àáâãäåæçèéêëìíîï"
#define Latin6 "ðñòóôõö÷øùúûüýþÿ"


/*Définition des structures*/
/*néant*/


/*---------------------------------Définition des Fonctions---------------------------------------*/



/* --------------------------------Execution du Main--------------------------------------------- */

int main(void)
{
	/*Déclarations*/
	int c;				//caractère lu
	int etat = 0;		//si à l’intérieur du mot = 1, sinon = 0
	int NbCa[Nmax];		//Nombre d’occurence par caractère
	int n = 0;			//compteur et indice de mot
	float moy = 0;		//taille moyenne des mots
	int max = 0;		//mot le plus long
	
	
	/*Instructions*/
	puts("Mettre fin à l’exercice en tapant sur «@».");
	puts("Attention : les caractères hors ascii peuvent compter ×2, ×3 ou ×4.");
	puts("\n   -      ×    ++  ***  ++    ×      -    \n");
	
	/*		Initialisation du compteur le longueur de mot*/
	for(int i = 0 ; i < Nmax ; i++)
		NbCa[i] = 0;
	
	
	/*		Comptage des longueurs*/
	c = getchar();
	
	while(c != '@')		//@ car, je ne peux pas générer EOF sur le terminal.
	{
		if((c != ' ') && (c != '\t') && (c != '\n'))
		{
			etat = 1;
			
			while(etat == 1)
			{
				NbCa[n]++;
				c = getchar();
				
				if((c == ' ') || (c == '\t') || (c == '\n') || (c == '@'))
					etat = 0;
			}
			
			n++;
		}
		
		else
			c = getchar();
	}
	
	/*		Affichage du barregraphe*/
	printf("********************************************\n\n");
	
	printf("Histogramme des %d glyphes saisis:\n\n", n);
	
	for(int p = 0 ; p < n ; p++)
	{
		if(NbCa[p] > max)
			max = NbCa[p];
	}
	
	for(int p = 0 ; p < n ; p++)
		NbCa[p] = NbCa[p] - max + 1;
	
	printf("\n");
	
	for(int j = 0 ; j < max ; j++)
	{
		for(int m = 0 ; m < n ; m++)
		{
			if(NbCa[m] > 0)
				printf("  |");
			else
				printf("   ");
		}
		
		for(int x = 0 ; x < n ; x++)
			NbCa[x]++;
		
		printf("\n");
	}
	
	for(int p = 0 ; p < n ; p++)
		printf("%3d", NbCa[p]-1);
	
	/*		Afficher la taille moyenne des mots saisis*/
	for(int k = 0 ; k < n ; k++)
		moy = moy + NbCa[k] - 1;
	moy = (float) moy / n;
	
	printf("\nLa taille moyenne des mots est de : %.3f\n", moy);
	
	
	/*fin programme*/
	return 0;		//pas d’erreur
}

/**************************************************************************************************/
